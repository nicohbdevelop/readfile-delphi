unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, CourseUnit, StudentUnit;

type
  TForm1 = class(TForm)
    Open: TButton;
    memo1: TMemo;
    Clear: TButton;
    DNI: TButton;
    openDialog1: TOpenDialog;
    procedure OpenClick(Sender: TObject);
    procedure DNIClick(Sender: TObject);
    procedure ClearClick(Sender: TObject);
  private
    { Private declarations }
    students : TList;
    procedure setStudents(students: TList);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
procedure TForm1.OpenClick(Sender: TObject);
var coursee: Course;
begin
  memo1.lines.clear;
  openDialog1.execute();
  coursee := Course.Create(openDialog1.fileName);
  students := coursee.get();
  self.setStudents(students);
end;

procedure TForm1.setStudents(students: TList);
  var i: integer;
  studentt : Student;
  nombre: String;
  begin
    for i := 0 to students.Count-1 do
      begin
        studentt := students[i];
        nombre := studentt.getNombre();
        memo1.lines.add(nombre);
      end;

end;

procedure TForm1.ClearClick(Sender: TObject);
begin
  memo1.lines.Clear;
end;

procedure TForm1.DNIClick(Sender: TObject);
var
    pointer : TPoint;
    selectedLine : Integer;
    selectedStudent: Student;
    nombre: String;
    dni: String;

  begin
    pointer := Memo1.CaretPos;
    selectedLine := pointer.Y;

    selectedStudent := students[selectedLine];
    nombre := selectedStudent.getNombre();
    dni := selectedStudent.getDni();

    showMessage('Nombre: ' + nombre + ' DNI: ' + dni);
end;

end.
