unit StudentUnit;

interface
type
  Student = class
    private
      nombre: String;
      dni: String;

    public
      constructor Create();
      function getNombre(): String;
      procedure setNombre(nombre: String);
      function getDni(): String;
      procedure setDni(dni: String);

  end;

implementation
 uses
      SysUtils, StrUtils;


  constructor Student.Create();
  begin
  end;

   function Student.getNombre(): String;
   begin
    result := self.nombre;
   end;

   function Student.getDni(): String;
   begin
    result := self.dni;
   end;
   procedure Student.setNombre(nombre: String);
   begin
    self.nombre := nombre;
   end;
   procedure Student.setDni(dni: String);
   begin
    self.dni := dni;
   end;




end.
