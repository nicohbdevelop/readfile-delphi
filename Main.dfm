object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Open: TButton
    Left = 96
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 0
    OnClick = OpenClick
  end
  object Memo1: TMemo
    Left = 232
    Top = 32
    Width = 353
    Height = 153
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object Clear: TButton
    Left = 288
    Top = 224
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 2
    OnClick = ClearClick
  end
  object DNI: TButton
    Left = 456
    Top = 224
    Width = 75
    Height = 25
    Caption = 'DNI'
    TabOrder = 3
    OnClick = DNIClick
  end
  object OpenDialog1: TOpenDialog
    Left = 120
    Top = 128
  end
end
